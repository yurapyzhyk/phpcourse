<?php

include_once ('vendor/autoload.php');

\App\App::run();


/*
 * $farm = new Farm();

        $farmer = $farm->getCEO();

        $farm->addAnimal(new Cow());
        $farm->addAnimal(new Chicken());

        $farm->addPlant(new Tree());
        $farm->addPlant(new Flower());

        foreach ($farm->getAllAnimals() as $animal) {
            $farmer->feedAnimal($animal);
            $animal->move();
            echo "As a result we've got ";
            $animal->giveProduct();
            echo "\n";
        }

        foreach ($farm->getAllPlants() as $plant) {
            $farmer->waterPlant($plant);
            echo "And now: ";
            $plant->harvest();
            echo "\n";

            try {
                $plant->setWeight(0);
            } catch (NoSuchPropertyException $e) {
                MyLogger::getInstance()->error($e->getMessage());
            }

            try {
                $plant->setHeight(-2);
            } catch (WrongPropertyValueException $e) {
                MyLogger::getInstance()->error($e->getMessage());
            }
        }
 */