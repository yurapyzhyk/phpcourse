<?php

namespace App\API;

interface ControllerInterface
{
    public function execute($request, $response);
}