<?php

namespace App\Farm\FarmHabitant;

/**
 * Class Chicken
 */
class Chicken extends AbstractAnimal
{
    /**
     * @inheritDoc
     */
    public function giveProduct()
    {
        echo "Egg\n";
    }
}