<?php

namespace App\Farm\FarmHabitant;

use App\Farm\Api\GrowableInterface;
use App\Farm\NoSuchPropertyException;
use App\Farm\WrongPropertyValueException;

abstract class AbstractHabitant implements GrowableInterface
{
    public function __call($name, $arguments)
    {
        $type = substr($name, 0 , 3);

        switch ($type) {
            case "set":
                $property = lcfirst(substr($name, 3));
                if ($property == "height")
                {
                    if ($arguments[0] <= 0) {
                        throw new WrongPropertyValueException("You've tried to set the wrong value [" . $arguments[0] . "] to property [$property]\n");
                    }
                } else {
                    throw new NoSuchPropertyException("There is no such property [$property]\n");
                }
                break;
        }
    }
}