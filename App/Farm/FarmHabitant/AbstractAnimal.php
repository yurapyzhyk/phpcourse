<?php

namespace App\Farm\FarmHabitant;

/**
 * Class AbstractAnimal
 */
abstract class AbstractAnimal extends AbstractHabitant
{
    /**
     * @var float
     */
    protected $height = 50.0;

    /**
     * Collect animal's product
     * @return void
     */
    abstract public function giveProduct();

    /**
     * Animal makes a move
     */
    public function move()
    {
        echo "Makes some move\n";
    }

    /**
     * A new animal is born
     * @return void;
     */
    public function born()
    {
        echo "A new animal was born\n";
    }

    /**
     * Grows an animal
     * @return void
     */
    public function grow()
    {
        $this->height += self::ANIMAL_GROWTH_FACTOR;
        echo "An animal grew by " . self::ANIMAL_GROWTH_FACTOR . " cm and now it has height " . $this->height ." cm\n";
    }
}