<?php

namespace App\Farm\FarmHabitant;

/**
 * Class Tree
 */
class Tree extends AbstractPlant
{
    /**
     * @inheritDoc
     */
    public function harvest()
    {
        echo "Fruits were collected\n";
    }
}