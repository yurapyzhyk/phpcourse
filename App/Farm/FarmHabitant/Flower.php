<?php

namespace App\Farm\FarmHabitant;

/**
 * Class Flower
 */
class Flower extends AbstractPlant
{
    /**
     * @inheritDoc
     */
    public function harvest()
    {
        echo "A flower was plucked\n";
    }
}