<?php

namespace App\Farm\FarmHabitant;

/**
 * Class AbstractPlant
 */
abstract class AbstractPlant extends AbstractHabitant
{
    /**
     * Initial plant's height in cm
     * @var int
     */
    protected $height = 10;

    /**
     * Collect plant's goods
     * @return void
     */
    abstract public function harvest();

    /**
     * Plants new plant
     * @return void
     */
    public function plant()
    {
        echo "New plant was planted\n";
    }

    /**
     * Grows a plant
     * @return void
     */
    public function grow()
    {
        $this->height += self::PLANT_GROWTH_FACTOR;
        echo "A plant grew by " . self::PLANT_GROWTH_FACTOR . " cm and now it has height " . $this->height ." cm\n";
    }
}