<?php

namespace App\Farm\FarmHabitant;

/**
 * Class Cow
 */
class Cow extends AbstractAnimal
{
    /**
     * @inheritDoc
     */
    public function giveProduct()
    {
        echo "Milk\n";
    }
}