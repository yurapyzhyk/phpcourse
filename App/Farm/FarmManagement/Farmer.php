<?php

namespace App\Farm\FarmManagement;

use App\Farm\FarmHabitant\AbstractPlant;
use App\Farm\FarmHabitant\AbstractAnimal;

/**
 * Class Farmer
 * Implements Singletone pattern
 */
class Farmer
{
    /**
     * An instance of farmer
     * @var Farmer | null
     */
    private static $farmer = null;

    /**
     * Get an instance of farmer
     * @return Farmer|null
     */
    public static function getInstance()
    {
        if (self::$farmer === null) {
            self::$farmer = new self();
        }

        return self::$farmer;
    }

    /**
     * Plant new plant on a farm
     * @param AbstractPlant $plant
     * @return void
     */
    public function plantPlant(AbstractPlant $plant)
    {
        $plant->plant();
    }

    /**
     * Get new animal for a farm
     * @param AbstractAnimal $animal
     * @return void
     */
    public function getNewAnimal(AbstractAnimal $animal)
    {
        $animal->born();
    }

    /**
     * Water plant to speed its growth
     * @param AbstractPlant $plant
     * @return void
     */
    public function waterPlant(AbstractPlant $plant)
    {
        $plant->grow();
    }

    /**
     * Feed animal to speed its growth
     * @param AbstractAnimal $animal
     * @return void
     */
    public function feedAnimal(AbstractAnimal $animal)
    {
        $animal->grow();
    }
}