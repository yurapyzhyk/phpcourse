<?php

namespace App\Farm\FarmManagement;

use App\Farm\FarmHabitant\AbstractPlant;
use App\Farm\FarmHabitant\AbstractAnimal;


/**
 * Class Farm
 */
class Farm
{
    /**
     * An array of all animals
     * @var array
     */
    private $animals = [];

    /**
     * An array of all plants
     * @var array
     */
    private $plants = [];

    /**
     * Farm constructor.
     */
    public function __construct()
    {
        echo "A farm was initialized\n";
    }

    /**
     * Get farmer of current farm
     * @return Farmer|null
     */
    public function getCEO()
    {
        return Farmer::getInstance();
    }

    /**
     * Get all plants of farm
     * @return array
     */
    public function getAllPlants()
    {
        return $this->plants;
    }


    /**
     * Get all animals of farm
     * @return array
     */
    public function getAllAnimals()
    {
        return $this->animals;
    }

    /**
     * Add a new animal to farm
     * @param AbstractAnimal $animal
     * @return void
     */
    public function addAnimal(AbstractAnimal $animal)
    {
        $this->getCEO()->getNewAnimal($animal);
        array_push($this->animals, $animal);
    }

    /**
     * Add a new plant to farm
     * @param AbstractPlant $plant
     * @return void
     */
    public function addPlant(AbstractPlant $plant)
    {
        $this->getCEO()->plantPlant($plant);
        array_push($this->plants, $plant);
    }
}