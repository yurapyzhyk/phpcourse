<?php

namespace App\Farm\Api;

interface GrowableInterface
{
    const ANIMAL_GROWTH_FACTOR = 0.1;
    const PLANT_GROWTH_FACTOR = 0.5;

    public function grow();
}