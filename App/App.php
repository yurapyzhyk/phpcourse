<?php

namespace App;

use App\Service\Router;

class App
{
    public static function run()
    {
        $router = new Router();
        $router->dispatch();
    }
}