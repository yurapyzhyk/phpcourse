<?php

namespace App;

use Katzgrau\KLogger\Logger;

class MyLogger
{
    const LOG_PATH = 'var/log';

    private static $logger = null;

    public static function getInstance() {
        if (self::$logger == null) {
            self::$logger = new Logger(self::LOG_PATH);
        }

        return self::$logger;
    }


    public function logInfo($message = "")
    {
        self::$logger->info($message);
    }

    public function logError($message = "")
    {
        self::$logger->error($message);
    }
}
