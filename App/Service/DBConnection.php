<?php

namespace App\Service;

use Medoo\Medoo;

class DBConnection
{
    private static $connection = null;

    public static function getConnection()
    {
        if (self::$connection == null) {
            self::$connection = new Medoo([
                'database_type' => 'mysql',
                'database_name' => 'farm',
                'server' => 'localhost',
                'username' => 'dbUser',
                'password' => 'password'
            ]);
        }

        return self::$connection;
    }
}