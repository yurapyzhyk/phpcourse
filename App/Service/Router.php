<?php

namespace App\Service;

use App\API\ControllerInterface;
use Klein\Klein;

class Router
{
    public $routes = ['method' => "GET",
        'path' => "/",
        'className' => \App\Controller\HomeController::class];

    public function dispatch()
    {
        $klein = new Klein();

        $route = $this->routes;

        $klein->respond(
            $route['method'],
            $route['path'],
            function ($request, $response) use ($route) {
                $controller = new $route['className']();
                if ($controller instanceof ControllerInterface) {
                    return $controller->execute($request, $response);
                } else {
                    throw new \Exception("Controller doesn't exist");
                }
            }
        );
        $klein->dispatch();
    }
}