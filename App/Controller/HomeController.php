<?php

namespace App\Controller;

use App\API\ControllerInterface;
use App\Service\DBConnection;

class HomeController implements ControllerInterface
{
    public function execute($request, $response)
    {
        $database = DBConnection::getConnection();

        $data = $database->select('animal', [
            'id',
            'name',
            'type',
            'created'
        ]);
        echo json_encode($data);
    }

}

class ListController implements ControllerInterface
{
    private $product;
    private $twig;
    public function __construct(
        Product $product,
        Environment $twig
    )
    {
        $this->product = $product;
        $this->twig = $twig;
    }

    public function execute($request, $response)
    {
        try {
            $this->product = Database::GetEntityManager()
                ->getRepository(Product::class)
                ->findAll();

            if(!$this->product)
            {
                throw new NotFoundException();
            }

            var_dump($this->product);die;

            $renderParams = array('data'=>$this->product);

            $template = $this->twig->load('Product.html');
            return $template->render($renderParams);

        } catch (NotFoundException $e) {
            $template = $this->twig->load('noresults.html');
            return  $template->render('noresults.html');

        } catch (\Exception $exception) {
            echo  $exception->getMessage();
        }
    }
}
